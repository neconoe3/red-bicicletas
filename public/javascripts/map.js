var map = L.map('main_map').setView([4.698710, -74.100137], 18);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);


$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function (result) {
        console.log(result);
        result.bicicletas.forEach(function (bici) {
            L.marker(bici.ubicacion, { title: bici.id }).addTo(map).bindPopup('Aqui justo ahora.<br>'+bici.id);
        });
    }
})

/*
L.marker([4.698710, -74.100137]).addTo(map).bindPopup('Aqui justo ahora.<br> Uno.').openPopup();
L.marker([4.698910, -74.100137]).addTo(map).bindPopup('Aqui justo ahora.<br> Dos.').openPopup();
L.marker([4.698390, -74.100197]).addTo(map).bindPopup('Aqui justo ahora.<br> Tres.').openPopup();
*/